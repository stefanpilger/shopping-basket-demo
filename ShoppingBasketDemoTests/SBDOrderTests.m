//
//  SBDOrderTests.m
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 29/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SBDOrder.h"

@interface SBDOrderTests : XCTestCase

@end

@implementation SBDOrderTests

- (void)testAddGoodsItem {
    SBDOrder *order = [SBDOrder new];
    SBDGoodsItem *goodsItem = [SBDGoodsItem itemWithItemName:@"TestItem" valuePerUnitInGBP:1.0 standardUnitName:@"unit" standardUnitNeedsOf:YES imageName:nil];
    XCTAssertThrows(order.positions[0]);
    
    [order addGoodsItem:goodsItem];
    XCTAssertNotNil(order.positions[0]);
}

- (void)testRemovePosition {
    SBDOrder *order = [SBDOrder new];
    SBDGoodsItem *goodsItem = [SBDGoodsItem itemWithItemName:@"TestItem" valuePerUnitInGBP:1.0 standardUnitName:@"unit" standardUnitNeedsOf:YES imageName:nil];
    [order addGoodsItem:goodsItem];
    [order removePositionAtIndex:0];
    XCTAssert(order.positions.count == 0);
}

- (void)testSetQuantity {
    SBDOrder *order = [SBDOrder new];
    SBDGoodsItem *goodsItem = [SBDGoodsItem itemWithItemName:@"TestItem" valuePerUnitInGBP:1.0 standardUnitName:@"unit" standardUnitNeedsOf:YES imageName:nil];
    [order addGoodsItem:goodsItem];
    XCTAssert(((SBDOrderPosition *)order.positions[0]).quantity == 1);
    
    [order setQuantity:3 forPositionAtIndex:0];
    XCTAssert(((SBDOrderPosition *)order.positions[0]).quantity == 3);
}
@end
