//
//  SBDPriceTest.m
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 29/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SBDPrice.h"

@interface SBDPriceTests : XCTestCase

@end

@implementation SBDPriceTests

- (void)testInit {
    SBDPrice *price = [[SBDPrice alloc] initWithValueInGBP:20.0 currency:[SBDCurrency currencyGBP]];
    XCTAssertNotNil(price);
}

- (void)testFactoryMethod {
    SBDPrice *price = [SBDPrice priceWithValueInGBP:20.0 currency:[SBDCurrency currencyGBP]];
    XCTAssertNotNil(price);
}

- (void)testValueInCurrency {
    SBDPrice *price = [SBDPrice priceWithValueInGBP:20.0 currency:[SBDCurrency currencyGBP]];
    XCTAssert(price.valueInCurrency == 20.0);
}

- (void)testFormattedString {
    SBDPrice *price = [SBDPrice priceWithValueInGBP:20.0 currency:[SBDCurrency currencyGBP]];
    XCTAssert([price.formattedString isEqualToString:@"£20.00"]);
}
@end
