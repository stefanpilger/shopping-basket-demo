//
//  SBDShopTests.m
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 29/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SBDShop.h"
#import "SBDGoodsItem.h"

@interface SBDShopTests : XCTestCase

@end

@implementation SBDShopTests

- (void)testInit {
    SBDShop *shop = [[SBDShop alloc] init];
    XCTAssert(shop);
    XCTAssert(shop.goods.count ==4);
    XCTAssert(((SBDGoodsItem *)shop.goods[0]).name = @"peas");
}

@end
