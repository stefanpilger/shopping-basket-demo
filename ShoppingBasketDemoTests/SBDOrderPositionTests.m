//
//  SBDOrderPositionTests.m
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 29/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SBDOrderPosition.h"

@interface SBDOrderPositionTests : XCTestCase

@end

@implementation SBDOrderPositionTests

- (void)testNaturalDescription {
    SBDGoodsItem *testItem = [SBDGoodsItem itemWithItemName:@"TestItem" valuePerUnitInGBP:2.0 standardUnitName:@"unit" standardUnitNeedsOf:YES imageName:nil];
    SBDOrderPosition *testPosition = [SBDOrderPosition positionWithGoodsItem:testItem quantity:10];
    XCTAssert([testPosition.naturalDescription isEqualToString:@"10 units of TestItem"]);
}

- (void)testValueInGBP {
    SBDGoodsItem *testItem = [SBDGoodsItem itemWithItemName:@"TestItem" valuePerUnitInGBP:2.0 standardUnitName:@"unit" standardUnitNeedsOf:YES imageName:nil];
    SBDOrderPosition *testPosition = [SBDOrderPosition positionWithGoodsItem:testItem quantity:10];
    XCTAssert(testPosition.valueInGBP == 20.0);
}

@end
