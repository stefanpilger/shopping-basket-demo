//
//  SBDCurrencyPickerController.h
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 27/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBDCurrency.h"

@protocol SBDCurrencyPickerControllerDelegate <NSObject>
- (void)currencyPickerDataSourceWasUpdated;
- (void)currencyPickerDidSelectCurrency:(SBDCurrency *)currency;
@end

@interface SBDCurrencyPickerController : NSObject<UIPickerViewDelegate, UIPickerViewDataSource>

+ (instancetype)controllerWithDelegate:(id<SBDCurrencyPickerControllerDelegate>)delegate;

- (instancetype)initWithDelegate:(id<SBDCurrencyPickerControllerDelegate>)delegate;

@property (strong, nonatomic) NSArray *currencies;
@property (weak, nonatomic) id <SBDCurrencyPickerControllerDelegate> delegate;
@end
