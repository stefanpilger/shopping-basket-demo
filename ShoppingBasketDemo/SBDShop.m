//
//  SBDShop.m
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 26/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import "SBDShop.h"

@implementation SBDShop

- (instancetype)init {
    if (self = [super init]) {
        
        // Hard-coded data for demo purposes. Usually you would find something like a CoreData database here...
        
        SBDGoodsItem *peas = [SBDGoodsItem itemWithItemName:@"peas"
                                          valuePerUnitInGBP:0.95
                                           standardUnitName:@"bag"
                                        standardUnitNeedsOf:YES
                                                  imageName:@"peas"];
        SBDGoodsItem *eggs = [SBDGoodsItem itemWithItemName:@"eggs"
                                          valuePerUnitInGBP:2.10
                                           standardUnitName:@"dozen"
                                        standardUnitNeedsOf:NO
                                                  imageName:@"eggs"];
        eggs.standardUnitPlural = @"dozen";
        SBDGoodsItem *milk = [SBDGoodsItem itemWithItemName:@"milk"
                                          valuePerUnitInGBP:1.30
                                           standardUnitName:@"bottle"
                                        standardUnitNeedsOf:YES
                                                  imageName:@"milk"];
        SBDGoodsItem *beans = [SBDGoodsItem itemWithItemName:@"beans"
                                           valuePerUnitInGBP:0.73
                                            standardUnitName:@"can"
                                         standardUnitNeedsOf:YES
                                                   imageName:@"beans"];
        self.goods = @[peas, eggs, milk, beans];
    }
    return self;
}

@end
