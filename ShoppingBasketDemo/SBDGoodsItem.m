//
//  SBDGoodsItem.m
//  ShoppingBasket
//
//  Created by Stefan Pilger on 25/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import "SBDGoodsItem.h"

@interface SBDGoodsItem ()
@end

@implementation SBDGoodsItem

+ (instancetype)itemWithItemName:(NSString *)itemName valuePerUnitInGBP:(double)valuePerUnitInGBP standardUnitName:(NSString *)standardUnitName standardUnitNeedsOf:(BOOL)standardUnitNeedsOf imageName:(NSString *)imageName {
    return [[self alloc] initWithItemName:itemName valuePerUnitInGBP:valuePerUnitInGBP standardUnitName:standardUnitName standardUnitNeedsOf:(BOOL)standardUnitNeedsOf imageName:imageName];
}

- (instancetype)initWithItemName:(NSString *)itemName valuePerUnitInGBP:(double)valuePerUnitInGBP standardUnitName:(NSString *)standardUnitName standardUnitNeedsOf:(BOOL)standardUnitNeedsOf imageName:(NSString *)imageName {
    if (self = [self init]) {
        self.name = itemName;
        self.valuePerUnitInGBP = valuePerUnitInGBP;
        self.standardUnit = standardUnitName;
        self.standardUnitNeedsOf = standardUnitNeedsOf;
        self.itemImage = [UIImage imageNamed:imageName];
    }
    return self;
}

- (NSString *)standardUnitPlural {
    // teaching the iPhone grammar...
    if (!_standardUnitPlural) {
        _standardUnitPlural = [self.standardUnit stringByAppendingString:@"s"];
    }
    return _standardUnitPlural;
}

- (SBDPrice *)priceInCurrency:(SBDCurrency *)currency {
    return [SBDPrice priceWithValueInGBP:self.valuePerUnitInGBP currency:currency];
}

@end
