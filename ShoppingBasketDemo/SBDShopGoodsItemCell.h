//
//  SBDShopGoodsItemCell.h
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 26/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBDShopGoodsItemCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemPricePerUnitLabel;

@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;

@end
