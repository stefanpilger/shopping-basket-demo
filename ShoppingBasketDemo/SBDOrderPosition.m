//
//  SBDOrderPosition.m
//  ShoppingBasket
//
//  Created by Stefan Pilger on 26/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import "SBDOrderPosition.h"

@implementation SBDOrderPosition

+ (instancetype)positionWithGoodsItem:(SBDGoodsItem *)goodsItem quantity:(NSInteger)quantity {
    return [[self alloc] initWithGoodsItem:goodsItem quantity:quantity];
}

- (instancetype)initWithGoodsItem:(SBDGoodsItem *)goodsItem quantity:(NSInteger)quantity {
    if (self = [self init]) {
        self.goodsItem = goodsItem;
        self.quantity = quantity;
    }
    return self;
}

- (NSString *)naturalDescription {
    return [NSString stringWithFormat:@"%li %@ %@%@",
            (long)self.quantity,
            self.quantity == 1? self.goodsItem.standardUnit : self.goodsItem.standardUnitPlural,
            self.goodsItem.standardUnitNeedsOf? @"of " : @"",
            self.goodsItem.name];
}

- (double)valueInGBP {
    return self.goodsItem.valuePerUnitInGBP * self.quantity;
}

- (SBDPrice *)priceInCurrency:(SBDCurrency *)currency {
    return [SBDPrice priceWithValueInGBP:self.valueInGBP currency:currency];
}

@end
