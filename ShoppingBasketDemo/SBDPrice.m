//
//  SBDPrice.m
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 26/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import "SBDPrice.h"

@implementation SBDPrice

+ (instancetype)priceWithValueInGBP:(double)valueInGBP currency:(SBDCurrency *)currency {
    return [[self alloc] initWithValueInGBP:valueInGBP currency:currency];
}

- (instancetype)initWithValueInGBP:(double)valueInGBP currency:(SBDCurrency *)currency {
    if (self = [self init]) {
        self.valueInGBP = valueInGBP;
        self.currency = currency;
    }
    return self;
}

- (double)valueInCurrency {
    return self.valueInGBP * self.currency.conversionRatePerGBP;
}

- (NSString *)formattedString {
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    formatter.numberStyle = NSNumberFormatterCurrencyStyle;
    formatter.currencySymbol = self.currency.symbol;
    return [formatter stringFromNumber:@(self.valueInCurrency)];
}

@end
