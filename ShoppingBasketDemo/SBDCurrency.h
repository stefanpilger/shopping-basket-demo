//
//  SBDCurrency.h
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 26/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBDCurrency : NSObject

+ (instancetype)currencyGBP;

- (instancetype)initWithName:(NSString *)name symbol:(NSString *)symbol code:(NSString *)code conversionRatePerGBP:(double)conversionRatePerGBP;
- (instancetype)initWithName:(NSString *)name code:(NSString *)code conversionRatePerGBP:(double)conversionRatePerGBP;

- (NSComparisonResult)sortByCode:(SBDCurrency *)otherCurrency;

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *symbol;
@property (strong, nonatomic) NSString *code;
@property (assign, nonatomic) double conversionRatePerGBP;
@end
