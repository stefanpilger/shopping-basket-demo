//
//  AppDelegate.h
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 26/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBDShop.h"
#import "SBDOrder.h"

@interface SBDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) SBDShop *shop;
@property (strong, nonatomic) SBDOrder *order;
@end

