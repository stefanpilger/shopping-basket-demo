//
//  SBDCurrency.m
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 26/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import "SBDCurrency.h"

@implementation SBDCurrency

+ (instancetype)currencyGBP {
    return [[self alloc] initWithName:@"Pound Sterling" symbol:@"£" code:@"GBP" conversionRatePerGBP:1.0];
}

- (instancetype)initWithName:(NSString *)name symbol:(NSString *)symbol code:(NSString *)code conversionRatePerGBP:(double)conversionRatePerGBP {
    if (self = [self initWithName:name code:code conversionRatePerGBP:conversionRatePerGBP]) {
        self.symbol = symbol;
    }
    return self;
}

- (instancetype)initWithName:(NSString *)name code:(NSString *)code conversionRatePerGBP:(double)conversionRatePerGBP {
    if (self = [self init]) {
        self.name = name;
        self.code = code;
        self.conversionRatePerGBP = conversionRatePerGBP;
    }
    return self;
}

- (NSString *)symbol {
    if (!_symbol) {
        _symbol = self.code;
    }
    return _symbol;
}

- (NSComparisonResult)sortByCode:(SBDCurrency *)otherCurrency {
    return [self.code compare:otherCurrency.code];
}

@end
