//
//  SBDShoppingBasketViewController.h
//  ShoppingBasket
//
//  Created by Stefan Pilger on 25/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBDOrder.h"
#import "SBDShoppingBasketGoodsItemCell.h"
#import "SBDCurrencyPickerController.h"

@interface SBDShoppingBasketViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate, SBDShoppingBasketGoodsItemCellDelegate, SBDCurrencyPickerControllerDelegate>
@property (strong, nonatomic) SBDOrder *order;
@end
