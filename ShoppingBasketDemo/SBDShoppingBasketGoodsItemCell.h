//
//  SBDShoppingBasketGoodsItemCell.h
//  ShoppingBasket
//
//  Created by Stefan Pilger on 25/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBDOrderPosition.h"

@class SBDShoppingBasketGoodsItemCell;
@protocol SBDShoppingBasketGoodsItemCellDelegate <NSObject>
- (void)stepperValueChangedInCell:(SBDShoppingBasketGoodsItemCell *)cell;
@end

@interface SBDShoppingBasketGoodsItemCell : UITableViewCell

@property (weak, nonatomic) id <SBDShoppingBasketGoodsItemCellDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIStepper *stepper;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemUnitPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemPriceLabel;
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;

@end
