//
//  SBDGoodsItem.h
//  ShoppingBasket
//
//  Created by Stefan Pilger on 25/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SBDPrice.h"

@interface SBDGoodsItem : NSObject

+ (instancetype)itemWithItemName:(NSString *)itemName valuePerUnitInGBP:(double)valuePerUnitInGBP standardUnitName:(NSString *)standardUnitName standardUnitNeedsOf:(BOOL)standardUnitNeedsOf imageName:(NSString *)imageName;

- (instancetype)initWithItemName:(NSString *)itemName valuePerUnitInGBP:(double)valuePerUnitInGBP standardUnitName:(NSString *)standardUnitName standardUnitNeedsOf:(BOOL)standardUnitNeedsOf imageName:(NSString *)imageName;

- (SBDPrice *)priceInCurrency:(SBDCurrency *)currency;

@property (strong, nonatomic) NSString *name;

@property (strong, nonatomic) UIImage *itemImage;

@property (strong, nonatomic) NSString *standardUnit;
@property (strong, nonatomic) NSString *standardUnitPlural;
@property (assign, nonatomic) BOOL standardUnitNeedsOf;

@property (assign, nonatomic) double valuePerUnitInGBP;
@end
