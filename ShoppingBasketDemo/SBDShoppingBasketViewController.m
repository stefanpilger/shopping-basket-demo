//
//  SBDShoppingBasketViewController.m
//  ShoppingBasket
//
//  Created by Stefan Pilger on 25/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import "SBDShoppingBasketViewController.h"
#import "SBDAppDelegate.h"
#import "SBDShoppingBasketTotalCell.h"

@interface SBDShoppingBasketViewController ()
@property (strong, nonatomic) SBDCurrency *displayCurrency;
@property (weak, nonatomic) IBOutlet UIPickerView *currencyPickerView;
@property (strong, nonatomic) SBDCurrencyPickerController *currencyPickerController;
@end

@implementation SBDShoppingBasketViewController

- (id<UIPickerViewDelegate, UIPickerViewDataSource>)currencyPickerController {
    if (!_currencyPickerController) {
        _currencyPickerController = [SBDCurrencyPickerController controllerWithDelegate:self];
    }
    return _currencyPickerController;
}

- (void)viewDidLoad {
    SBDAppDelegate *appDelegate = UIApplication.sharedApplication.delegate;
    self.order = appDelegate.order;
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(dataSourceWasUpdatedWithNotification:) name:@"OrderWasUpdated" object:nil];
    [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(connectionErrorWithNotification:) name:@"CurrencyConnectionError" object:nil];
    
    self.currencyPickerView.hidden = YES;
    self.currencyPickerView.delegate = self.currencyPickerController;
    self.currencyPickerView.dataSource = self.currencyPickerController;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // when there are no order positions we need 1 line to tell the user the order is empty
    // when there are order positions, we need 2 more lines for total and currency switching
    NSInteger count = self.order.positions.count;
    return (count == 0)? 1 : count + 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell;
    
    if (self.order.positions.count == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"Empty"];
    } else {
        if (indexPath.row < self.order.positions.count) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"GoodsItem"];
            [self populateShoppingBasketGoodsItemCell:(SBDShoppingBasketGoodsItemCell *)cell withOrderPosition:self.order.positions[indexPath.row]];
        } else if (indexPath.row == self.order.positions.count) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"OrderTotal"];
            ((SBDShoppingBasketTotalCell *)cell).totalPriceLabel.text = [@"Total: " stringByAppendingString:self.order.totalPrice.formattedString];
        } else if (indexPath.row == self.order.positions.count + 1) {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Currency"];
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    // this enables deletion of order items by swiping
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.order removePositionAtIndex:indexPath.row];
    }
}

- (void)populateShoppingBasketGoodsItemCell:(SBDShoppingBasketGoodsItemCell *)cell withOrderPosition:(SBDOrderPosition *)orderPosition {
    cell.delegate = self;
    cell.stepper.value = orderPosition.quantity;
    cell.itemNameLabel.text = orderPosition.naturalDescription;
    cell.itemUnitPriceLabel.text = [NSString stringWithFormat:@"@ %@ per %@",
                                    [orderPosition.goodsItem priceInCurrency:self.order.currency].formattedString,
                                    orderPosition.goodsItem.standardUnit];
    cell.itemImageView.image = orderPosition.goodsItem.itemImage;
    cell.itemPriceLabel.text = [orderPosition priceInCurrency:self.order.currency].formattedString;
}

- (void)dataSourceWasUpdatedWithNotification:(NSNotification *)notification {
    [self.tableView reloadData];
}

- (void)stepperValueChangedInCell:(SBDShoppingBasketGoodsItemCell *)cell {
    // update quantity in order
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    [self.order setQuantity:cell.stepper.value forPositionAtIndex:indexPath.row];
}

- (IBAction)currencyButtonTouched:(UIButton *)sender {
    self.currencyPickerView.hidden = !self.currencyPickerView.hidden;
}

- (void)currencyPickerDataSourceWasUpdated {
    // reload currency picker and set to currently selected currency
    [self.currencyPickerView reloadAllComponents];
    NSInteger currencyIndex = [self.currencyPickerController.currencies indexOfObjectPassingTest:^BOOL(id obj, NSUInteger idx, BOOL *stop) {
        return [((SBDCurrency *)obj).code isEqualToString:self.order.currency.code];
    }];
    if (currencyIndex < self.currencyPickerController.currencies.count) {
        [self.currencyPickerView selectRow:currencyIndex inComponent:0 animated:YES];
    }
}

- (void)currencyPickerDidSelectCurrency:(SBDCurrency *)currency {
    self.order.currency = currency;
}

- (void)connectionErrorWithNotification:(NSNotification *)notification {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection error" message:@"Attempt to retrieve currencies failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

@end
