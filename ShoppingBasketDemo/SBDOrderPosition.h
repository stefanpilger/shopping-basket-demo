//
//  SBDOrderPosition.h
//  ShoppingBasket
//
//  Created by Stefan Pilger on 26/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBDGoodsItem.h"

@interface SBDOrderPosition : NSObject

+ (instancetype)positionWithGoodsItem:(SBDGoodsItem *)goodsItem quantity:(NSInteger)quantity ;

- (instancetype)initWithGoodsItem:(SBDGoodsItem *)goodsItem quantity:(NSInteger)quantity;

- (NSString *)naturalDescription;
- (double)valueInGBP;
- (SBDPrice *)priceInCurrency:(SBDCurrency *)currency;

@property (strong, nonatomic) SBDGoodsItem *goodsItem;
@property (assign, nonatomic) NSInteger quantity;
@end
