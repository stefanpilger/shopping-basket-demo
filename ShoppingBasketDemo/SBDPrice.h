//
//  SBDPrice.h
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 26/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBDCurrency.h"

@interface SBDPrice : NSObject

+ (instancetype)priceWithValueInGBP:(double)value currency:(SBDCurrency *)currency;

- (instancetype)initWithValueInGBP:(double)value currency:(SBDCurrency *)currency;

- (double)valueInCurrency;
- (NSString *)formattedString;

@property (strong, nonatomic) SBDCurrency *currency;
@property (assign, nonatomic) double valueInGBP;
@end
