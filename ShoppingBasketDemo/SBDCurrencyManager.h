//
//  SBDCurrencyManager.h
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 28/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBDCurrencyManager : NSObject

- (void)requestListOfCurrencies;
- (void)requestConversionRateForCurrencyCode:(NSString *)currencyCode;

@property (strong, nonatomic) NSDictionary *currencyDictionary;
@end
