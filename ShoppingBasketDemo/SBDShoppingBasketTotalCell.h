//
//  SBDShoppingBasketTotalCell.h
//  ShoppingBasket
//
//  Created by Stefan Pilger on 25/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SBDShoppingBasketTotalCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@end
