//
//  main.m
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 26/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBDAppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SBDAppDelegate class]));
    }
}
