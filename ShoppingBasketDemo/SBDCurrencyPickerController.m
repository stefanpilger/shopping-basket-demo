//
//  SBDCurrencyPickerController.m
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 27/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import "SBDCurrencyPickerController.h"
#import "SBDCurrencyManager.h"

@interface SBDCurrencyPickerController ()
@property (strong, nonatomic) SBDCurrency *selectedCurrency;
@property (strong, nonatomic) SBDCurrencyManager *currencyManager;
@end

@implementation SBDCurrencyPickerController

+ (instancetype)controllerWithDelegate:(id<SBDCurrencyPickerControllerDelegate>)delegate {
    return [[self alloc] initWithDelegate:delegate];
}

- (instancetype)initWithDelegate:(id<SBDCurrencyPickerControllerDelegate>)delegate {
    if (self = [self init]) {
        [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(currencyListWasUpdatedWithNotification:) name:@"CurrencyListUpdated" object:nil];
        [NSNotificationCenter.defaultCenter addObserver:self selector:@selector(currencyConversionRateReceivedWithNotification:) name:@"CurrencyConversionRateReceived" object:nil];
        [self.currencyManager requestListOfCurrencies];
        self.delegate = delegate;
    }
    return self;
}

- (SBDCurrencyManager *)currencyManager {
    if (!_currencyManager) {
        _currencyManager = [SBDCurrencyManager new];
    }
    return _currencyManager;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [NSString stringWithFormat:@"%@: %@", ((SBDCurrency *)self.currencies[row]).code, ((SBDCurrency *)self.currencies[row]).name];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.currencies.count;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.selectedCurrency = self.currencies[row];
    [self.currencyManager requestConversionRateForCurrencyCode:self.selectedCurrency.code];
}

- (void)currencyListWasUpdatedWithNotification:(NSNotification *)notification {
    NSDictionary *currencyDictionary = self.currencyManager.currencyDictionary;
    
    // create array of currency codes and names for picker
    NSMutableArray *newCurrencies = [NSMutableArray new];
    for (NSString *key in currencyDictionary.allKeys) {
        SBDCurrency *newCurrency = [SBDCurrency new];
        newCurrency.code = key;
        [newCurrencies addObject:newCurrency];
    }
    for (SBDCurrency *newCurrency in newCurrencies) {
        newCurrency.name = currencyDictionary[newCurrency.code];
    }
    
    self.currencies = [newCurrencies sortedArrayUsingSelector:@selector(sortByCode:)];
    [self.delegate currencyPickerDataSourceWasUpdated];
}

- (void)currencyConversionRateReceivedWithNotification:(NSNotification *)notification {
    // update currency object with conversion rate
    self.selectedCurrency.conversionRatePerGBP = ((NSNumber *)notification.userInfo[@"ConversionRate"]).doubleValue;
    [self.delegate currencyPickerDidSelectCurrency:self.selectedCurrency];
}

@end
