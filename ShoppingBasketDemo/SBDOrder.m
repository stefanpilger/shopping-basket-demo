//
//  SBDOrder.m
//  ShoppingBasket
//
//  Created by Stefan Pilger on 26/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import "SBDOrder.h"

@interface SBDOrder ()
@property (strong, nonatomic) NSMutableArray *mutablePositions;
@end

@implementation SBDOrder
@synthesize currency = _currency;

- (SBDCurrency *)currency {
    if (!_currency) {
        _currency = [SBDCurrency currencyGBP];
    }
    return _currency;
}

- (NSMutableArray *)mutablePositions {
    if (!_mutablePositions) {
        _mutablePositions = [NSMutableArray arrayWithCapacity:4];
    }
    return _mutablePositions;
}

- (void)setCurrency:(SBDCurrency *)currency {
    _currency = currency;
    [self broadcastUpdate];
}

- (void)addGoodsItem:(SBDGoodsItem *)goodsItem {
    // don't create duplicates but increase count of existing item
    BOOL added = NO;
    for (SBDOrderPosition *position in self.mutablePositions) {
        if ([position.goodsItem isEqual:goodsItem]) {
            position.quantity++;
            added = YES;
        }
    }
    if (!added) {
        [self.mutablePositions addObject:[SBDOrderPosition positionWithGoodsItem:goodsItem quantity:1]];
    }
    [self broadcastUpdate];
}

- (void)removePositionAtIndex:(NSUInteger)index {
    [self.mutablePositions removeObjectAtIndex:index];
    [self broadcastUpdate];
}

- (void)setQuantity:(NSInteger)quantity forPositionAtIndex:(NSUInteger)index {
    ((SBDOrderPosition *)self.mutablePositions[index]).quantity = quantity;
    [self broadcastUpdate];
}

- (NSArray *)positions {
    return self.mutablePositions;
}

- (double)totalValueInGBP {
    double total = 0;
    for (SBDOrderPosition *position in self.mutablePositions) {
        total += position.valueInGBP;
    }
    return total;
}

- (SBDPrice *)totalPrice {
    return [SBDPrice priceWithValueInGBP:self.totalValueInGBP currency:self.currency];
}

- (void)broadcastUpdate {
    [NSNotificationCenter.defaultCenter postNotificationName:@"OrderWasUpdated" object:nil];
}

@end
