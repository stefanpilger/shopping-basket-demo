//
//  SBDShopViewController.m
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 26/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import "SBDShopViewController.h"
#import "SBDShopGoodsItemCell.h"
#import "SBDAppDelegate.h"

@implementation SBDShopViewController

- (void)viewDidLoad {
    SBDAppDelegate *appDelegate = UIApplication.sharedApplication.delegate;
    self.shop = appDelegate.shop;
    self.order = appDelegate.order;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.shop.goods.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    SBDShopGoodsItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GoodsItem" forIndexPath:indexPath];
    SBDGoodsItem *goodsItem = self.shop.goods[indexPath.row];
    
    cell.itemNameLabel.text = goodsItem.name.capitalizedString;
    cell.itemPricePerUnitLabel.text = [NSString stringWithFormat:@"%@ per %@", [goodsItem priceInCurrency:self.order.currency].formattedString, goodsItem.standardUnit];
    cell.itemImageView.image = goodsItem.itemImage;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [self.order addGoodsItem:self.shop.goods[indexPath.row]];
}

@end
