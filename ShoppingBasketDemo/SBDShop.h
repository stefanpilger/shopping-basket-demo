//
//  SBDShop.h
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 26/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SBDGoodsItem.h"

@interface SBDShop : UICollectionViewCell
@property (strong, nonatomic) NSArray *goods;
@end
