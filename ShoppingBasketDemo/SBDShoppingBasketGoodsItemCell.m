//
//  SBDShoppingBasketGoodsItemCell.m
//  ShoppingBasket
//
//  Created by Stefan Pilger on 25/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import "SBDShoppingBasketGoodsItemCell.h"
#import "SBDCurrency.h"

@implementation SBDShoppingBasketGoodsItemCell

- (IBAction)stepperValueChanged:(UIStepper *)sender {
    [self.delegate stepperValueChangedInCell:self];
}

@end
