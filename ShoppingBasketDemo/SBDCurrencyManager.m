//
//  SBDCurrencyManager.m
//  ShoppingBasketDemo
//
//  Created by Stefan Pilger on 28/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import "SBDCurrencyManager.h"
#import "SBDCurrency.h"
#import "SBDConstants.h"

@implementation SBDCurrencyManager

- (void)requestListOfCurrencies {
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://jsonrates.com/currencies.json"]];
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (connectionError) {
            NSLog(@"Error while trying to download list of currencies: %@", connectionError);
            [NSNotificationCenter.defaultCenter postNotificationName:@"CurrencyConnectionError" object:nil];
        } else {
            self.currencyDictionary = [self decodeCurrencyDictionaryFromData:data];
            [NSNotificationCenter.defaultCenter postNotificationName:@"CurrencyListUpdated" object:nil];
        }
    }];
}

- (void)requestConversionRateForCurrencyCode:(NSString *)currencyCode {
    NSString *URLString = [NSString stringWithFormat:@"http://jsonrates.com/get/?from=GBP&to=%@&apiKey=%@", currencyCode, JSONRATES_API_KEY];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:URLString]];
    
    [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue currentQueue] completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (connectionError) {
            NSLog(@"Error while trying to get conversion rate: %@", connectionError);
            [NSNotificationCenter.defaultCenter postNotificationName:@"CurrencyConnectionError" object:nil];
        } else {
            double conversionRate = [self decodeConversionRateFromData:data];
            [NSNotificationCenter.defaultCenter postNotificationName:@"CurrencyConversionRateReceived" object:nil userInfo:@{@"ConversionRate": @(conversionRate)}];
        }
    }];
}

- (id)decodeJSONFromData:(NSData *)data {
    NSError *error = nil;
    id JSONObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    if (error) {
        NSLog(@"Error while trying to decode JSON: %@", error);
    }
    return JSONObject;
}

- (NSDictionary *)decodeCurrencyDictionaryFromData:(NSData *)currencyDictionaryData {
    return (NSDictionary *)[self decodeJSONFromData:currencyDictionaryData];
}

- (double)decodeConversionRateFromData:(NSData *)conversionRateData {
    NSDictionary *conversionRateDictionary = (NSDictionary *)[self decodeJSONFromData:conversionRateData];
    return ((NSNumber *)conversionRateDictionary[@"rate"]).doubleValue;
}




@end
