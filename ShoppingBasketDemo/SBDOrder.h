//
//  SBDOrder.h
//  ShoppingBasket
//
//  Created by Stefan Pilger on 26/04/2015.
//  Copyright (c) 2015 Tall Spires Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SBDOrderPosition.h"
#import "SBDPrice.h"

@interface SBDOrder : NSObject

- (void)addGoodsItem:(SBDGoodsItem *)goodsItem;
- (void)removePositionAtIndex:(NSUInteger)index;
- (void)setQuantity:(NSInteger)quantity forPositionAtIndex:(NSUInteger)index;

- (NSArray *)positions;
- (SBDPrice *)totalPrice;

@property (strong, nonatomic) SBDCurrency *currency;
@end
